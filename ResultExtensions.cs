using System;

namespace ResultExperiment
{
    public static class ResultEstensions
    {
        /// <summary>
        /// Invokes action if success
        /// </summary>
        public static Result OnSuccess(this Result result, Action action)
        {
            if (result.IsSuccess)
            {
                action();
            }

            return result;
        }

        /// <summary>
        /// Invokes action if failure
        /// </summary>
        public static Result OnFailure(this Result result, Action action)
        {
            if (result.IsFailure)
            {
                action();
            }

            return result;
        }

        /// <summary>
        /// Invokes action with result value if success
        /// </summary>
        public static Result<TValue, TError> OnSuccess<TValue, TError>(
            this Result<TValue, TError> result,
            Action<TValue> action)
        {
            if (result.IsSuccess)
            {
                action(result.Value);
            }

            return result;
        }

        /// <summary>
        /// Invokes action with result error if failure
        /// </summary>
        public static Result<TValue, TError> OnFailure<TValue, TError>(
            this Result<TValue, TError> result,
            Action<TError> action)
        {
            if (result.IsFailure)
            {
                action(result.Error);
            }

            return result;
        }

        /// <summary>
        /// Invokes next function on result value if success
        /// </summary>
        public static Result<TValueNext, TError> OnSuccess<TValue, TError, TValueNext>(
            this Result<TValue, TError> result,
            Func<TValue, Result<TValueNext, TError>> next)
        {
            if (result.IsFailure)
            {
                return Result.Fail(result.Error);
            }

            return next(result.Value);
        }

        /// <summary>
        /// Invokes next function on result value if success, otherwise maps result error
        /// </summary>
        public static Result<TValueNext, TErrorNext> OnSuccess<TValue, TError, TValueNext, TErrorNext>(
            this Result<TValue, TError> result,
            Func<TValue, Result<TValueNext, TErrorNext>> next,
            Func<TError, TErrorNext> mapError)
        {
            if (result.IsFailure)
            {
                return Result.Fail(mapError(result.Error));
            }

            return next(result.Value);
        }
    }
}