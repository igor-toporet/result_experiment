using System;

namespace ResultExperiment
{
    public class Result : IEquatable<Result>
    {
        protected Result(bool isSuccess)
        {
            IsSuccess = isSuccess;
        }

        public override string ToString() => IsSuccess? "Success": "Failure";

        public bool IsSuccess { get; }

        public bool IsFailure => !IsSuccess;

        public static Result Ok() => new Result(true);

        public static Success<TValue> Ok<TValue>(TValue value) =>
        new Success<TValue>(value);

        public static Result Fail() => new Result(false);

        public static Failure<TError> Fail<TError>(TError error) =>
        new Failure<TError>(error);

        public bool Equals(Result other) => ReferenceEquals(other, null) ? false : IsSuccess == other.IsSuccess;

        public override bool Equals(object obj) => Equals(obj as Result);

        public override int GetHashCode() => IsSuccess.GetHashCode();

        public static bool operator ==(Result a, Result b)
        {
            if (ReferenceEquals(a, null) && ReferenceEquals(b, null))
                return true;

            if (ReferenceEquals(a, null) || ReferenceEquals(b, null))
                return false;

            return a.Equals(b);
        }

        public static bool operator !=(Result a, Result b) => !(a == b);

        public class Success<TValue> : Result,
        IEquatable<Success<TValue>>
        {
            public override string ToString() => $"Success | {Value}";

            private readonly TValue _value;

            public TValue Value => IsSuccess?_value : throw new InvalidOperationException(
                "Operation was not successful, so there is no value available.");

            protected internal Success(TValue value) : base(true)
            {
                _value = value;
            }

            public override int GetHashCode() => new { IsSuccess, Value }.GetHashCode();

            public override bool Equals(object obj) => Equals(obj as Success<TValue>);

            public bool Equals(Success<TValue> other) => ReferenceEquals(other, null) ? false : IsSuccess == other.IsSuccess && Value.Equals(other.Value);

            public static bool operator ==(Success<TValue> a, Success<TValue> b)
            {
                if (ReferenceEquals(a, null) && ReferenceEquals(b, null))
                    return true;

                if (ReferenceEquals(a, null) || ReferenceEquals(b, null))
                    return false;

                return a.Equals(b);
            }

            public static bool operator !=(Success<TValue> a, Success<TValue> b) => !(a == b);
        }

        public class Failure<TError> : Result,
        IEquatable<Failure<TError>>
        {
            public override string ToString() => $"Failure | {Error}";

            public TError Error { get; }

            protected internal Failure(TError error) : base(false)
            {
                Error = error;
            }

            public override int GetHashCode() => new { IsSuccess, Error }.GetHashCode();

            public override bool Equals(object obj) => Equals(obj as Failure<TError>);

            public bool Equals(Failure<TError> other) =>
            ReferenceEquals(other, null) ? false : IsFailure == other.IsFailure && Error.Equals(other.Error);

            public static bool operator ==(Failure<TError> a, Failure<TError> b)
            {
                if (ReferenceEquals(a, null) && ReferenceEquals(b, null))
                    return true;

                if (ReferenceEquals(a, null) || ReferenceEquals(b, null))
                    return false;

                return a.Equals(b);
            }

            public static bool operator !=(Failure<TError> a, Failure<TError> b) => !(a == b);
        }
    }

    public class Result<TValue, TError> : Result
    {
        public override string ToString() => IsSuccess?$"Success | {Value}": $"Failure | {Error}";

        private readonly TValue _value;
        private readonly TError _error;

        protected internal Result(TValue value) : base(true)
        {
            _value = value;
        }

        protected internal Result(TError error) : base(false)
        {
            _error = error;
        }

        public TValue Value => IsSuccess?_value : throw new InvalidOperationException(
            "Operation was not successful, so there is no value available.");

        public TError Error => IsFailure ? _error : throw new InvalidOperationException(
            "Operation was successful, so there is no error available.");

        public static implicit operator Result<TValue, TError>(Failure<TError> failure) =>
            new Result<TValue, TError>(failure.Error);

        public static implicit operator Result<TValue, TError>(Success<TValue> success) =>
            new Result<TValue, TError>(success.Value);
    }
}