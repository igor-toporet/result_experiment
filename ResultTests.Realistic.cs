﻿using System;
using System.Linq;
using FluentAssertions;
using Xunit;

namespace ResultExperiment
{
    public class ResultTests_Realistic_Scenario_1
    {
        [Fact]
        public void List_of_failed_and_succesful_results()
        {
            var inputs = new [] { null, "123", "1234567", "1234567890abc" };

            var things = inputs.Select(x => GetStuff(x)).ToList();

            var expected = new []
            {
                "Failure | Invalid",
                "Failure | TooShort",
                "Success | 1234567",
                "Failure | TooLong"
            };

            Assert.Equal(expected, things.Select(x => x.ToString()));
        }

        private Result<Stuff, FailureKind> GetStuff(string input)
        {
            if (string.IsNullOrWhiteSpace(input)) return Result.Fail(FailureKind.Invalid);
            if (input.Length < 5) return Result.Fail(FailureKind.TooShort);
            if (input.Length > 10) return Result.Fail(FailureKind.TooLong);

            return Result.Ok(new Stuff { Thing = input });
        }

        private class Stuff
        {
            public string Thing { get; set; }

            public override string ToString() => Thing;
        }

        private enum FailureKind
        {
            Invalid,
            TooShort,
            TooLong
        }
    }

    public class ResultTests_Realistic_Scenario_2
    {
        [Fact]
        public void Railway_approach_same_type_passed_around()
        {
            var dirtyInput = new DirtyInputDto
            {
                DirtyA = new DtoA { A = "  hey  " },
                DirtyB = new DtoB { B = " Yow " },
                DirtyC = new DtoC { C = " Bro " }
            };

            var result = Init()
                .OnSuccess(v => SanitizeA(v, dirtyInput.DirtyA))
                .OnSuccess(v => SanitizeB(v, dirtyInput.DirtyB))
                .OnSuccess(v => SanitizeC(v, dirtyInput.DirtyC));

            var expected = new CleanInputModel
            {
                TrimmedA = "hey",
                LowercaseB = " yow ",
                CapitalizedC = " BRO "
            };

            result.IsSuccess.Should().BeTrue();
            result.Value.ShouldBeEquivalentTo(expected);
        }

        private Result<CleanInputModel, Oops> Init() => Result.Ok(new CleanInputModel());
        private Result<CleanInputModel, Oops> SanitizeA(CleanInputModel inputModel, DtoA dto)
        {
            if (string.IsNullOrWhiteSpace(dto.A)) return Result.Fail(Oops.UnableToTrim);

            inputModel.TrimmedA = dto.A.Trim();
            return Result.Ok(inputModel);
        }
        private Result<CleanInputModel, Oops> SanitizeB(CleanInputModel inputModel, DtoB dto)
        {
            if (string.IsNullOrWhiteSpace(dto.B)) return Result.Fail(Oops.UnableToLower);

            inputModel.LowercaseB = dto.B.ToLowerInvariant();
            return Result.Ok(inputModel);
        }
        private Result<CleanInputModel, Oops> SanitizeC(CleanInputModel inputModel, DtoC dto)
        {
            if (string.IsNullOrWhiteSpace(dto.C)) return Result.Fail(Oops.UnableToUpper);

            inputModel.CapitalizedC = dto.C.ToUpperInvariant();
            return Result.Ok(inputModel);
        }

        private class DtoA { public string A; }
        private class DtoB { public string B; }
        private class DtoC { public string C; }

        private class DirtyInputDto
        {
            public DtoA DirtyA;
            public DtoB DirtyB;
            public DtoC DirtyC;
        }

        private class CleanInputModel
        {
            public string TrimmedA;
            public string LowercaseB;
            public string CapitalizedC;
        }

        private enum Oops
        {
            UnableToTrim,
            UnableToLower,
            UnableToUpper
        }
    }
}