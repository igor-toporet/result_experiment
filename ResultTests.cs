﻿using System;
using System.Linq;
using FluentAssertions;
using Xunit;

namespace ResultExperiment
{
    public class ResultTests
    {
        [Fact]
        public void Ok_IsSuccess()
        {
            var result = Result.Ok();

            result.IsSuccess.Should().BeTrue();
            result.IsFailure.Should().BeFalse();
        }

        [Fact]
        public void Ok_with_value_IsSuccess()
        {
            Result.Ok(Guid.NewGuid()).IsSuccess.Should().BeTrue();
        }

        [Fact]
        public void Ok_with_value_has_Value()
        {
            const int value = 42;

            var result = Result.Ok(value);

            result.Value.Should().Be(value);
        }

        [Fact]
        public void Fail_IsFailure()
        {
            var result = Result.Fail();

            result.IsFailure.Should().BeTrue();
            result.IsSuccess.Should().BeFalse();
        }

        [Fact]
        public void Fail_with_error_IsFailure()
        {
            Result.Fail("an error").IsFailure.Should().BeTrue();
        }

        [Fact]
        public void Fail_with_error_has_Error()
        {
            const string error = "whoa!";

            var result = Result.Fail(error);

            result.Error.Should().Be(error);
        }

        [Fact]
        public void Implicit_cast_from_Failure_result()
        {
            Result<string, int> result = Result.Fail(-1);

            result.Should().NotBeNull();
        }

        [Fact]
        public void Implicit_cast_from_Failure_result_accessing_Value_throws()
        {
            Result<string, int> result = Result.Fail(-1);

            var exception = Record.Exception(() => result.Value);

            exception.Should().BeOfType<InvalidOperationException>()
                .Which
                .Message.Should().Be("Operation was not successful, so there is no value available.");
        }

        [Fact]
        public void Implicit_cast_from_Success_result_accessing_Error_throws()
        {
            Result<string, int> result = Result.Ok("done");

            var exception = Record.Exception(() => result.Error);

            exception.Should().BeOfType<InvalidOperationException>()
                .Which
                .Message.Should().Be("Operation was successful, so there is no error available.");
        }

        [Fact]
        public void Implicit_cast_from_Success_result()
        {
            Result<string, int> result = Result.Ok("abc");

            result.Should().NotBeNull();
            result.Value.Should().Be("abc");
        }

        [Fact]
        public void Implicit_cast_from_Failure_result_unambiguous_even_if_error_has_same_type_as_value()
        {
            Result<string, string> result = Result.Fail("whoa");

            result.Should().NotBeNull();
            result.IsFailure.Should().BeTrue();
            result.Error.Should().Be("whoa");
        }

        [Fact]
        public void Implicit_cast_from_Success_result_unambiguous_even_if_value_has_same_type_as_error()
        {
            Result<string, string> result = Result.Ok("fine");

            result.Should().NotBeNull();
            result.IsSuccess.Should().BeTrue();
            result.Value.Should().Be("fine");
        }
    }
}