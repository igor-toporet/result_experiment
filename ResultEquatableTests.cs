using FluentAssertions;
using Xunit;

namespace ResultExperiment
{
    public class ResultEquatableTests
    {
        [Fact]
        public void Result_Equatable_equality()
        {
            var r1 = Result.Ok();
            var r2 = Result.Ok();

            r1.Equals(r2).Should().BeTrue();
            r2.Equals(r1).Should().BeTrue();
            Equals(r1, r2).Should().BeTrue();
            Equals(r2, r1).Should().BeTrue();
            (r1 == r2).Should().BeTrue();
            (r2 == r1).Should().BeTrue();
            (r1 != r2).Should().BeFalse();
            (r2 != r1).Should().BeFalse();
        }

        [Fact]
        public void Result_Equatable_inequality()
        {
            var success = Result.Ok();
            var failure = Result.Fail();

            success.Equals(failure).Should().BeFalse();
            failure.Equals(success).Should().BeFalse();
            Equals(success, failure).Should().BeFalse();
            Equals(failure, success).Should().BeFalse();
            (success == failure).Should().BeFalse();
            (failure == success).Should().BeFalse();
            (success != failure).Should().BeTrue();
            (failure != success).Should().BeTrue();
        }

        [Fact]
        public void Success_Equatable_equality()
        {
            var s1 = Result.Ok(123);
            var s2 = Result.Ok(123);

            s1.Equals(s2).Should().BeTrue();
            s2.Equals(s1).Should().BeTrue();
            Equals(s1, s2).Should().BeTrue();
            Equals(s2, s1).Should().BeTrue();
            (s1 == s2).Should().BeTrue();
            (s2 == s1).Should().BeTrue();
            (s1 != s2).Should().BeFalse();
            (s2 != s1).Should().BeFalse();
        }

        [Fact]
        public void Success_Equatable_inequality()
        {
            var s1 = Result.Ok(123);
            var s2 = Result.Ok(456);

            s1.Equals(s2).Should().BeFalse();
            s2.Equals(s1).Should().BeFalse();
            Equals(s1, s2).Should().BeFalse();
            Equals(s2, s1).Should().BeFalse();
            (s1 == s2).Should().BeFalse();
            (s2 == s1).Should().BeFalse();
            (s1 != s2).Should().BeTrue();
            (s2 != s1).Should().BeTrue();
        }

        [Fact]
        public void Failure_Equatable_equality()
        {
            var f1 = Result.Fail(WhatIsWrong.Weather);
            var f2 = Result.Fail(WhatIsWrong.Weather);

            f1.Equals(f2).Should().BeTrue();
            f2.Equals(f1).Should().BeTrue();
            Equals(f1, f2).Should().BeTrue();
            Equals(f2, f1).Should().BeTrue();
            (f1 == f2).Should().BeTrue();
            (f2 == f1).Should().BeTrue();
            (f1 != f2).Should().BeFalse();
            (f2 != f1).Should().BeFalse();
        }

        [Fact]
        public void Failure_Equatable_inequality()
        {
            var f1 = Result.Fail(WhatIsWrong.Database);
            var f2 = Result.Fail(WhatIsWrong.Weather);

            f1.Equals(f2).Should().BeFalse();
            f2.Equals(f1).Should().BeFalse();
            Equals(f1, f2).Should().BeFalse();
            Equals(f2, f1).Should().BeFalse();
            (f1 == f2).Should().BeFalse();
            (f2 == f1).Should().BeFalse();
            (f1 != f2).Should().BeTrue();
            (f2 != f1).Should().BeTrue();
        }

        private enum WhatIsWrong
        {
            Database,
            Weather
        }
    }
}