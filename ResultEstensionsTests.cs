using System.Linq;
using System.Text;
using FluentAssertions;
using Xunit;

namespace ResultExperiment
{
    public class ResultEstensionsTests
    {
        [Fact]
        public void OnSuccess_Result_Action_produce_as_many_side_effects_as_you_like()
        {
            var log = new StringBuilder();

            Result.Ok()
                .OnSuccess(() => log.Append("foo"))
                .OnSuccess(() => log.Append("bar"));

            log.ToString().Should().Be("foobar");
        }

        [Fact]
        public void OnFailure_Result_Action_produce_as_many_side_effects_as_you_like()
        {
            var log = new StringBuilder();

            Result.Fail()
                .OnFailure(() => log.Append("argh!"))
                .OnFailure(() => log.Append("oops"));

            log.ToString().Should().Be("argh!oops");
        }

        [Fact]
        public void OnSuccess_Result_Value_Action_produce_as_many_side_effects_as_you_like()
        {
            var log = new StringBuilder();

            Result<string, int> result = Result.Ok("AB");

            result
                .OnSuccess(value => log.Append(value))
                .OnSuccess(value => log.Append(new string(value.Reverse().ToArray())));

            log.ToString().Should().Be("ABBA");
        }

        [Fact]
        public void OnFailure_Result_Error_Action_produce_as_many_side_effects_as_you_like()
        {
            var log = new StringBuilder();

            Result<string, int> result = Result.Fail(101);

            result
                .OnFailure(error => log.Append(error * 2))
                .OnFailure(error => log.Append(error * -3));

            log.ToString().Should().Be("202-303");
        }
    }
}